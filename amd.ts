import { available, unavailable } from "./notify.ts";
import check from "./check-availability.ts";
import { everyMinute } from "https://deno.land/x/deno_cron/cron.ts";

if (await check()) {
  available();
} else {
  unavailable();
}

everyMinute(async () => {
  if (await check()) {
    available();
  }
});
