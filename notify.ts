import { notify } from "https://deno.land/x/deno_notify@0.4.5/ts/mod.ts";

const plugin = new URL("./deno_notify.dll", import.meta.url).pathname.substring(
  1,
);
// Load the plugin manually
Deno.openPlugin(plugin);

export function available() {
  notify({
    title: "AMD GPU available!",
    message: "amd.com",
    icon: {
      app: "Terminal",
    },
    sound: "Basso",
  });
}

export function unavailable() {
  notify({
    title: "Unavailable",
    message: "Checking every minute",
    icon: {
      app: "Terminal",
    },
    sound: "Basso",
  });
}
