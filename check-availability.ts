import jsdom from "https://dev.jspm.io/jsdom";

export default async function () {
  let html = await (await fetch("https://www.amd.com/en/direct-buy/nl")).text();
  html = html.replace(/<img([\w\W]+?)[\/]?>/ig, "");

  //@ts-ignore
  const dom = new jsdom.JSDOM(html);
  const midnight =
    "div.views-row:nth-child(5) > div:nth-child(2) > div:nth-child(2) > div:nth-child(5)";
  const normal =
    "div.views-row:nth-child(11) > div:nth-child(2) > div:nth-child(2) > div:nth-child(5)";
  const midnight_stock = dom.window.document.querySelector(
    midnight,
  ).textContent.trim() !== "Out of Stock";
  const normal_stock = dom.window.document.querySelector(
    normal,
  ).textContent.trim() !== "Out of Stock";
  return normal_stock || midnight_stock;
}
