# 6800XT availability

Sends notification if a 6800XT is available on [AMD's website](https://www.amd.com/en/direct-buy/nl).
Checks every minute.

Run:

`deno run --unstable --allow-all amd.ts`

Or:

`deno run --unstable --allow-env --allow-read --allow-net --allow-plugin amd.ts`
